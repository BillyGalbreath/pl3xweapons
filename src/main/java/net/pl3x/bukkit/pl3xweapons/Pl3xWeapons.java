package net.pl3x.bukkit.pl3xweapons;

import net.pl3x.bukkit.pl3xweapons.command.CmdItem;
import net.pl3x.bukkit.pl3xweapons.command.CmdSpawnMoney;
import net.pl3x.bukkit.pl3xweapons.command.CmdWeapon;
import net.pl3x.bukkit.pl3xweapons.listener.CraftingListener;
import net.pl3x.bukkit.pl3xweapons.listener.ItemListener;
import net.pl3x.bukkit.pl3xweapons.listener.WeaponListener;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

public class Pl3xWeapons extends JavaPlugin implements Listener {
    @Override
    public void onEnable() {
        getServer().getPluginManager().registerEvents(new CraftingListener(), this);
        getServer().getPluginManager().registerEvents(new ItemListener(), this);
        getServer().getPluginManager().registerEvents(new WeaponListener(this), this);

        getCommand("item").setExecutor(new CmdItem(this));
        getCommand("spawnmoney").setExecutor(new CmdSpawnMoney(this));
        getCommand("weapon").setExecutor(new CmdWeapon(this));
    }
}
