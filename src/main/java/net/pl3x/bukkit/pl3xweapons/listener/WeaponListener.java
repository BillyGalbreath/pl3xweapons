package net.pl3x.bukkit.pl3xweapons.listener;

import net.pl3x.bukkit.pl3xweapons.Pl3xWeapons;
import net.pl3x.bukkit.pl3xweapons.util.NBTUtil;
import net.pl3x.bukkit.pl3xweapons.weapon.Weapon;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.ExperienceOrb;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.enchantment.EnchantItemEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.inventory.PrepareAnvilEvent;
import org.bukkit.event.inventory.PrepareItemCraftEvent;
import org.bukkit.event.player.PlayerExpChangeEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.CraftingInventory;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.metadata.MetadataValue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

public class WeaponListener implements Listener {
    private final Pl3xWeapons plugin;
    private final Set<UUID> swinging = new HashSet<>();

    public WeaponListener(Pl3xWeapons plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onAnvilPrepare(PrepareAnvilEvent event) {
        ItemStack result = event.getResult();
        Weapon weapon = Weapon.get(result);
        if (weapon == null) {
            return; // not enchanting a weapon
        }

        int level = result.getEnchantmentLevel(Enchantment.DAMAGE_ALL);
        if (level > 0) {
            // fix damage attribute
            // damage = damage + 1 + ((level - 1) * 0.5)
            result = NBTUtil.setAttackDamage(result,
                    weapon.getBaseAttack() + 1 + ((level - 1) * 0.5));
        }

        // fix colored name
        ItemMeta meta = result.getItemMeta();
        meta.setDisplayName((result.getEnchantments().size() > 0 ?
                ChatColor.AQUA : ChatColor.WHITE) + weapon.getName());
        result.setItemMeta(meta);

        // update result
        event.setResult(result);
        event.getView().setItem(2, result); // this is for updateInventory without delay

        for (HumanEntity human : event.getViewers()) {
            ((Player) human).updateInventory();
        }
    }

    @EventHandler
    public void onEnchant(EnchantItemEvent event) {
        ItemStack result = event.getItem();
        Weapon weapon = Weapon.get(result);
        if (weapon == null) {
            return; // not enchanting a weapon
        }

        Map<Enchantment, Integer> toAdd = event.getEnchantsToAdd();
        if (!toAdd.containsKey(Enchantment.DAMAGE_ALL)) {
            return;
        }

        // damage = damage + 1 + ((level - 1) * 0.5)
        result = NBTUtil.setAttackDamage(result,
                weapon.getBaseAttack() + 1 + ((toAdd.get(Enchantment.DAMAGE_ALL) - 1) * 0.5));
        event.getItem().setItemMeta(result.getItemMeta());
    }

    @EventHandler
    public void onPrepareCraft(PrepareItemCraftEvent event) {
        ItemStack[] matrix = event.getInventory().getMatrix();
        for (Weapon weapon : Weapon.values()) {
            if (weapon.getRecipe() != null && weapon.getRecipe().isRecipe(matrix)) {
                ItemStack item = weapon.getItem();
                plugin.getServer().getScheduler().runTaskLater(plugin, () -> {
                    event.getInventory().setResult(item);
                    for (HumanEntity human : event.getViewers()) {
                        ((Player) human).updateInventory();
                    }
                }, 1);
                return;
            }
        }
    }

    @EventHandler(ignoreCancelled = true)
    public void onCraft(InventoryClickEvent event) {
        if (!(event.getClickedInventory() instanceof CraftingInventory)) {
            return;
        }
        if (event.getSlotType() != InventoryType.SlotType.RESULT) {
            return;
        }
        CraftingInventory inventory = (CraftingInventory) event.getInventory();
        Weapon weapon = Weapon.get(inventory.getResult());
        if (weapon == null) {
            return; // not a weapon.
        }
        for (int i = 1; i < 10; i++) {
            ItemStack slot = inventory.getItem(i);
            if (slot != null) {
                if (slot.getAmount() == 1) {
                    inventory.setItem(i, new ItemStack(Material.AIR));
                } else {
                    slot.setAmount(slot.getAmount() - 1);
                    inventory.setItem(i, slot);
                }
            }
        }
        event.setCurrentItem(new ItemStack(Material.AIR));
        Player player = ((Player) event.getWhoClicked());
        plugin.getServer().getScheduler().runTaskLater(plugin, () -> {
            if (event.isShiftClick()) {
                player.getInventory().addItem(weapon.getItem());
                event.getView().setCursor(new ItemStack(Material.AIR));
            } else {
                event.getView().setCursor(weapon.getItem());
            }
            player.updateInventory();
        }, 1);
    }

    @EventHandler
    public void onPlayerPickupItem(PlayerPickupItemEvent event) {
        Item item = event.getItem();
        if (!item.hasMetadata("owner")) {
            return; // no metadata
        }

        List<MetadataValue> metaData = item.getMetadata("owner");
        if (metaData == null || metaData.isEmpty()) {
            return; // no metadata
        }

        MetadataValue metaValue = metaData.get(0);
        if (metaValue == null) {
            return; // no metadata
        }

        String owner = metaValue.asString();
        if (owner == null || owner.isEmpty() || owner.equalsIgnoreCase("none")) {
            return; // no owner
        }

        if (owner.equals(event.getPlayer().getUniqueId().toString())) {
            return; // owner picked up
        }

        // not owner, cancel pickup event
        event.setCancelled(true);
    }

    @EventHandler
    public void onDamageEntity(EntityDamageByEntityEvent event) {
        if (!(event.getDamager() instanceof Player)) {
            return;
        }

        Player player = ((Player) event.getDamager());
        if (swinging.contains(player.getUniqueId())) {
            return; // already swinging
        }

        ItemStack item = player.getInventory().getItemInMainHand();
        Weapon weapon = Weapon.get(item);
        if (weapon == null) {
            return;
        }

        swinging.add(player.getUniqueId());
        plugin.getServer().getScheduler().runTaskLater(plugin, () ->
                swinging.remove(player.getUniqueId()), 0);

        if (Weapon.decrementDurability(item, weapon.getMaxDurability()) < 0) {
            // break item
            player.getInventory().setItemInMainHand(new ItemStack(Material.AIR));
            player.getWorld().playSound(player.getLocation(), Sound.ENTITY_ITEM_BREAK, 1F, 1F);
        }
    }

    @EventHandler
    public void onPickupExp(PlayerExpChangeEvent event) {
        if (event.getAmount() <= 0) {
            return; // not gaining exp
        }
        if (!(event.getSource() instanceof ExperienceOrb)) {
            return; // not from exp orb
        }

        // get all gear with mending
        Map<ItemStack, EquipmentSlot> items = new HashMap<>();
        ItemStack item = event.getPlayer().getInventory().getItemInMainHand();
        if (item != null && item.containsEnchantment(Enchantment.MENDING)) {
            items.put(item, EquipmentSlot.HAND);
        }
        item = event.getPlayer().getInventory().getItemInOffHand();
        if (item != null && item.containsEnchantment(Enchantment.MENDING)) {
            items.put(item, EquipmentSlot.OFF_HAND);
        }
        item = event.getPlayer().getInventory().getBoots();
        if (item != null && item.containsEnchantment(Enchantment.MENDING)) {
            items.put(item, EquipmentSlot.FEET);
        }
        item = event.getPlayer().getInventory().getLeggings();
        if (item != null && item.containsEnchantment(Enchantment.MENDING)) {
            items.put(item, EquipmentSlot.LEGS);
        }
        item = event.getPlayer().getInventory().getChestplate();
        if (item != null && item.containsEnchantment(Enchantment.MENDING)) {
            items.put(item, EquipmentSlot.CHEST);
        }
        item = event.getPlayer().getInventory().getHelmet();
        if (item != null && item.containsEnchantment(Enchantment.MENDING)) {
            items.put(item, EquipmentSlot.HEAD);
        }

        if (items.isEmpty()) {
            return; // nothing has mending
        }

        // get random piece of gear
        List<ItemStack> keys = new ArrayList<>(items.keySet());
        item = keys.get(ThreadLocalRandom.current().nextInt(keys.size()));
        EquipmentSlot itemSlot = items.get(item);

        // check if weapon
        Weapon weapon = Weapon.get(item);
        if (weapon == null) {
            return; // not a weapon. let vanilla handle mending
        }

        // check if can be repaired
        int durability = Weapon.getDurability(item);
        if (durability >= weapon.getMaxDurability()) {
            // does not need repair
            // give player the exp to prevent vanilla
            // from re-rolling mending chance on other gear
            event.getPlayer().giveExp(event.getAmount());
            event.setAmount(0);
            return;
        }

        // add 2 durability per exp
        durability += event.getAmount() * 2;
        event.setAmount(0);

        // do not over repair
        if (durability > weapon.getMaxDurability()) {
            durability = weapon.getMaxDurability();
        }

        // repair
        Weapon.setDurability(item, durability, weapon.getMaxDurability());

        // give repaired item back
        switch (itemSlot) {
            case HAND:
                event.getPlayer().getInventory().setItemInMainHand(item);
                break;
            case OFF_HAND:
                event.getPlayer().getInventory().setItemInOffHand(item);
                break;
            case FEET:
                event.getPlayer().getInventory().setBoots(item);
                break;
            case LEGS:
                event.getPlayer().getInventory().setLeggings(item);
                break;
            case CHEST:
                event.getPlayer().getInventory().setChestplate(item);
                break;
            case HEAD:
                event.getPlayer().getInventory().setHelmet(item);
        }

        // update the view
        event.getPlayer().updateInventory();
    }
}
