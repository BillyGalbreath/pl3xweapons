package net.pl3x.bukkit.pl3xweapons.listener;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.PrepareItemCraftEvent;
import org.bukkit.inventory.CraftingInventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class CraftingListener implements Listener {
    @EventHandler(priority = EventPriority.LOWEST)
    public void onCraft(PrepareItemCraftEvent event) {
        CraftingInventory inv = event.getInventory();

        ItemStack result = inv.getResult();
        if (result == null || result.getType() == Material.AIR) {
            return;
        }

        if (result.getType() != Material.DIAMOND_HOE &&
                result.getType() != Material.DIAMOND_SWORD) {
            return;
        }

        List<ItemStack> items = new ArrayList<>();
        for (ItemStack item : inv.getMatrix()) {
            if (item != null && item.getType() != Material.AIR) {
                items.add(item);
            }
        }

        if (items.size() == 2 && items.get(0).isSimilar(items.get(1))) {
            inv.setResult(null);
        }
    }
}
