package net.pl3x.bukkit.pl3xweapons.listener;

import net.pl3x.bukkit.pl3xweapons.item.CustomItem;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

import java.util.concurrent.ThreadLocalRandom;

public class ItemListener implements Listener {
    @EventHandler
    public void onBlockBreak(BlockBreakEvent event) {
        Block block = event.getBlock();
        if (block.getType() != Material.GLOWING_REDSTONE_ORE) {
            return; // not breaking redstone
        }

        ItemStack hand = event.getPlayer().getInventory().getItemInMainHand();
        if (hand.containsEnchantment(Enchantment.SILK_TOUCH)) {
            return; // silk touch never drops ruby
        }

        double chance = 0.01D;
        double fortune = hand.getEnchantmentLevel(Enchantment.LOOT_BONUS_BLOCKS);
        double rng = ThreadLocalRandom.current().nextDouble(0D, 100D);

        if (rng < (fortune + 1) * chance) {
            block.getWorld().dropItem(block.getLocation(), CustomItem.RUBY.getItem());
        }
    }
}
