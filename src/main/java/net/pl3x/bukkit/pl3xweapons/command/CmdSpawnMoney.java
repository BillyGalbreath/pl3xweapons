package net.pl3x.bukkit.pl3xweapons.command;

import net.pl3x.bukkit.pl3xweapons.Pl3xWeapons;
import net.pl3x.bukkit.pl3xweapons.item.CustomItem;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Item;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ThreadLocalRandom;

public class CmdSpawnMoney implements TabExecutor {
    private final Pl3xWeapons plugin;
    public final NumberFormat FORMAT = NumberFormat.getCurrencyInstance(Locale.US);

    public CmdSpawnMoney(Pl3xWeapons plugin) {
        this.plugin = plugin;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        return Collections.emptyList();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!sender.hasPermission("command.spawnmoney")) {
            sender.sendMessage(ChatColor.RED + "You dont have permission for that command");
            return true;
        }

        if (args.length < 1) {
            sender.sendMessage(ChatColor.RED + "Please specify an amount");
            return true;
        }

        if (args.length < 5) {
            sender.sendMessage(ChatColor.RED + "Please specify a location");
            return true;
        }

        World world = Bukkit.getWorld(args[1]);
        if (world == null) {
            sender.sendMessage(ChatColor.RED + "Invalid world specified!");
            return true;
        }

        double x, y, z;
        try {
            x = Double.parseDouble(args[2]);
            y = Double.parseDouble(args[3]);
            z = Double.parseDouble(args[4]);
        } catch (NumberFormatException e) {
            sender.sendMessage(ChatColor.RED + "Invalid location specified!");
            return true;
        }

        double amount;
        try {
            amount = Double.parseDouble(args[0]);
        } catch (NumberFormatException e) {
            sender.sendMessage(ChatColor.RED + "Invalid amount specified!");
            return true;
        }

        ItemStack stack = amount >= 1.0D ? CustomItem.DOLLAR.getItem() : CustomItem.COIN.getItem();
        ItemMeta meta = stack.getItemMeta();
        String formattedAmount = FORMAT.format(amount);

        List<String> lore = new ArrayList<>();
        lore.add("Pl3xMoney");
        lore.add("Amount: " + formattedAmount);
        lore.add(String.valueOf(ThreadLocalRandom.current().nextInt()));
        meta.setLore(lore);

        stack.setItemMeta(meta);

        Location location = new Location(world, x, y, z);
        Item item = location.getWorld().dropItem(location, stack);
        item.setCustomName(formattedAmount);
        item.setCustomNameVisible(true);
        item.setCanMobPickup(false);
        return true;
    }
}
