package net.pl3x.bukkit.pl3xweapons.command;

import net.pl3x.bukkit.pl3xweapons.Pl3xWeapons;
import net.pl3x.bukkit.pl3xweapons.weapon.Weapon;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class CmdWeapon implements TabExecutor {
    private final Pl3xWeapons plugin;

    public CmdWeapon(Pl3xWeapons plugin) {
        this.plugin = plugin;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        if (args.length == 1) {
            return Bukkit.getOnlinePlayers().stream()
                    .filter(player -> player.getName().toLowerCase().startsWith(args[0].toLowerCase()))
                    .map(Player::getName).collect(Collectors.toList());
        }
        if (args.length == 2) {
            return Arrays.stream(Weapon.values())
                    .filter(weapon -> weapon.getName().toLowerCase().startsWith(args[1].toLowerCase()))
                    .map(Weapon::name).collect(Collectors.toList());
        }
        return Collections.emptyList();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!sender.hasPermission("command.weapon")) {
            sender.sendMessage(ChatColor.RED + "You dont have permission for that command");
            return true;
        }
        if (args.length < 1) {
            sender.sendMessage(ChatColor.RED + "Please specify a player");
            return true;
        }
        if (args.length < 2) {
            sender.sendMessage(ChatColor.RED + "Please specify a weapon to give");
            return true;
        }

        Player target = Bukkit.getPlayer(args[0]);
        if (target == null) {
            sender.sendMessage(ChatColor.RED + "Player not found");
            return true;
        }

        Weapon weapon = Weapon.get(args[1]);
        if (weapon == null) {
            sender.sendMessage(ChatColor.RED + "Weapon not found");
            return true;
        }

        Item item = target.getWorld().dropItem(target.getLocation(), weapon.getItem());
        item.setMetadata("owner", new FixedMetadataValue(plugin, target.getUniqueId().toString()));
        return true;
    }
}
