package net.pl3x.bukkit.pl3xweapons.weapon;

import net.pl3x.bukkit.pl3xweapons.recipes.BaseRecipe;
import net.pl3x.bukkit.pl3xweapons.recipes.EmeraldSwordRecipe;
import net.pl3x.bukkit.pl3xweapons.recipes.RubySwordRecipe;
import net.pl3x.bukkit.pl3xweapons.util.NBTUtil;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

public enum Weapon {
    EMERALD_SWORD(Material.DIAMOND_SWORD, 0, "Emerald Sword", 10, 1776, new EmeraldSwordRecipe()),
    RUBY_SWORD(Material.DIAMOND_SWORD, 1, "Ruby Sword", 15, 2038, new RubySwordRecipe()),
    FIRE_SWORD(Material.DIAMOND_SWORD, 2, "Fire Sword", 20, 768, null);

    private static Weapon[] BY_ID = new Weapon[3];
    private static final Map<String, Weapon> BY_NAME = new HashMap<>();

    static {
        for (Weapon weapon : values()) {
            if (BY_ID.length > weapon.id) {
                BY_ID[weapon.id] = weapon;
            } else {
                BY_ID = Arrays.copyOfRange(BY_ID, 0, weapon.id + 2);
                BY_ID[weapon.id] = weapon;
            }
            BY_NAME.put(weapon.name(), weapon);
        }
    }

    public static Weapon get(int id) {
        return BY_ID.length > id && id >= 0 ? BY_ID[id] : null;
    }

    public static Weapon get(String name) {
        return BY_NAME.get(name);
    }

    public static Weapon get(ItemStack item) {
        for (Weapon weapon : values()) {
            if (weapon.isWeapon(item)) {
                return weapon;
            }
        }
        return null;
    }

    private ItemStack item;
    private int id;
    private String name;
    private double baseAttack;
    private int maxDurability;
    private BaseRecipe recipe;

    Weapon(Material material, int id, String name, double baseAttack, int maxDurability, BaseRecipe recipe) {
        this.id = id;
        this.name = name;
        this.baseAttack = baseAttack;
        this.maxDurability = maxDurability;
        this.recipe = recipe;

        item = new ItemStack(material, 1, (short) (id + 101));

        ItemMeta meta = item.getItemMeta();
        meta.setUnbreakable(true);
        meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
        meta.setDisplayName(ChatColor.WHITE + name);
        List<String> lore = new ArrayList<>();
        lore.add(ChatColor.GRAY + "Durability: " + maxDurability + " / " + maxDurability);
        meta.setLore(lore);
        item.setItemMeta(meta);

        item = NBTUtil.setAttackDamage(item, baseAttack);
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public double getBaseAttack() {
        return baseAttack;
    }

    public int getMaxDurability() {
        return maxDurability;
    }

    public BaseRecipe getRecipe() {
        return recipe;
    }

    public ItemStack getItem() {
        return item.clone();
    }

    public boolean isWeapon(ItemStack stack) {
        if (stack == null ||
                stack.getType() != item.getType() ||
                stack.getDurability() != 101 + id ||
                !stack.hasItemMeta()) {
            return false;
        }
        ItemMeta meta = stack.getItemMeta();
        return meta.isUnbreakable() &&
                meta.hasItemFlag(ItemFlag.HIDE_UNBREAKABLE);
    }

    public static int decrementDurability(ItemStack stack, int maxDurability) {
        int durability = getDurability(stack);
        if (stack.containsEnchantment(Enchantment.DURABILITY) &&
                100D / (stack.getEnchantmentLevel(Enchantment.DURABILITY) + 1D) <
                        ThreadLocalRandom.current().nextDouble(0, 100)) {
            return durability; // unbreaking kicked in. no durability loss
        }
        return setDurability(stack, --durability, maxDurability);
    }

    public static int getDurability(ItemStack stack) {
        ItemMeta meta = stack.getItemMeta();
        List<String> lore = meta.hasLore() ? meta.getLore() : new ArrayList<>();
        int durability = Integer.MIN_VALUE;
        for (String line : lore) {
            if (line.startsWith(ChatColor.GRAY + "Durability: ")) {
                try {
                    durability = Integer.parseInt(line.split(" ")[1]);
                    break;
                } catch (NumberFormatException ignore) {
                }
            }
        }
        return durability;
    }

    public static int setDurability(ItemStack stack, int durability, int maxDurability) {
        ItemMeta meta = stack.getItemMeta();
        List<String> lore = meta.hasLore() ? meta.getLore() : new ArrayList<>();
        boolean edited = false;
        for (int i = 0; i < lore.size(); i++) {
            if (lore.get(i).startsWith(ChatColor.GRAY + "Durability: ")) {
                lore.set(i, ChatColor.GRAY + "Durability: " + durability + " / " + maxDurability);
                edited = true;
            }
        }
        if (!edited) {
            lore.add(ChatColor.GRAY + "Durability: " + durability + " / " + maxDurability);
        }
        meta.setLore(lore);
        stack.setItemMeta(meta);
        return durability;
    }
}
