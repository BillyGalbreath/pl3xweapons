package net.pl3x.bukkit.pl3xweapons.recipes;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class EmeraldSwordRecipe extends BaseRecipe {
    public EmeraldSwordRecipe() {
        ItemStack emerald = new ItemStack(Material.EMERALD);
        ItemStack stick = new ItemStack(Material.STICK);

        ItemStack[] recipe = new ItemStack[9];
        recipe[0] = emerald;
        recipe[3] = emerald;
        recipe[6] = stick;
        addRecipe(recipe);

        recipe = new ItemStack[9];
        recipe[1] = emerald;
        recipe[4] = emerald;
        recipe[7] = stick;
        addRecipe(recipe);

        recipe = new ItemStack[9];
        recipe[2] = emerald;
        recipe[5] = emerald;
        recipe[8] = stick;
        addRecipe(recipe);
    }
}
