package net.pl3x.bukkit.pl3xweapons.recipes;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class RubySwordRecipe extends BaseRecipe {
    public RubySwordRecipe() {
        ItemStack ruby = new ItemStack(Material.DIAMOND_HOE, 1, (short) 105);
        ItemStack stick = new ItemStack(Material.STICK);

        ItemMeta meta = ruby.getItemMeta();
        meta.setUnbreakable(true);
        meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        meta.setDisplayName(ChatColor.WHITE + "Ruby");
        ruby.setItemMeta(meta);

        ItemStack[] recipe = new ItemStack[9];
        recipe[0] = ruby;
        recipe[3] = ruby;
        recipe[6] = stick;
        addRecipe(recipe);

        recipe = new ItemStack[9];
        recipe[1] = ruby;
        recipe[4] = ruby;
        recipe[7] = stick;
        addRecipe(recipe);

        recipe = new ItemStack[9];
        recipe[2] = ruby;
        recipe[5] = ruby;
        recipe[8] = stick;
        addRecipe(recipe);
    }
}
