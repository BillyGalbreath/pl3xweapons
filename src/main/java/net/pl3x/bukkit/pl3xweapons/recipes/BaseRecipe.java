package net.pl3x.bukkit.pl3xweapons.recipes;

import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.HashSet;
import java.util.Set;

public abstract class BaseRecipe {
    private final Set<ItemStack[]> recipes = new HashSet<>();

    public void addRecipe(ItemStack[] recipe) {
        recipes.add(recipe);
    }

    public Set<ItemStack[]> getRecipes() {
        return recipes;
    }

    public void clearRecipes() {
        recipes.clear();
    }

    public boolean isRecipe(ItemStack[] matrix) {
        for (ItemStack[] recipe : recipes) {
            boolean match = true;
            for (int i = 0; i < 9; i++) {
                if (matrix[i] == null && recipe[i] == null) {
                    continue; // both empty
                }
                if (matrix[i] == null || recipe[i] == null) {
                    match = false; // one is empty while other is not
                    break;
                }
                if (matrix[i].getType() == recipe[i].getType() &&
                        matrix[i].getDurability() == recipe[i].getDurability()) {
                    if ((matrix[i].hasItemMeta() && !recipe[i].hasItemMeta()) ||
                            (!matrix[i].hasItemMeta() && recipe[i].hasItemMeta())) {
                        match = false; // one has meta while other does not
                        break;
                    }
                    if (matrix[i].hasItemMeta() && recipe[i].hasItemMeta()) {
                        ItemMeta recipeMeta = recipe[i].getItemMeta();
                        ItemMeta matrixMeta = matrix[i].getItemMeta();
                        if ((matrixMeta.isUnbreakable() && !recipeMeta.isUnbreakable()) ||
                                (!matrixMeta.isUnbreakable() && recipeMeta.isUnbreakable())) {
                            match = false; // one is unbreakable while other is not
                            break;
                        }
                        if ((matrixMeta.hasItemFlag(ItemFlag.HIDE_UNBREAKABLE) && !recipeMeta.hasItemFlag(ItemFlag.HIDE_UNBREAKABLE)) ||
                                (!matrixMeta.hasItemFlag(ItemFlag.HIDE_UNBREAKABLE) && recipeMeta.hasItemFlag(ItemFlag.HIDE_UNBREAKABLE)) ||
                                (matrixMeta.hasItemFlag(ItemFlag.HIDE_ATTRIBUTES) && !recipeMeta.hasItemFlag(ItemFlag.HIDE_ATTRIBUTES)) ||
                                (!matrixMeta.hasItemFlag(ItemFlag.HIDE_ATTRIBUTES) && recipeMeta.hasItemFlag(ItemFlag.HIDE_ATTRIBUTES)) ||
                                (matrixMeta.hasItemFlag(ItemFlag.HIDE_DESTROYS) && !recipeMeta.hasItemFlag(ItemFlag.HIDE_DESTROYS)) ||
                                (!matrixMeta.hasItemFlag(ItemFlag.HIDE_DESTROYS) && recipeMeta.hasItemFlag(ItemFlag.HIDE_DESTROYS)) ||
                                (matrixMeta.hasItemFlag(ItemFlag.HIDE_ENCHANTS) && !recipeMeta.hasItemFlag(ItemFlag.HIDE_ENCHANTS)) ||
                                (!matrixMeta.hasItemFlag(ItemFlag.HIDE_ENCHANTS) && recipeMeta.hasItemFlag(ItemFlag.HIDE_ENCHANTS)) ||
                                (matrixMeta.hasItemFlag(ItemFlag.HIDE_PLACED_ON) && !recipeMeta.hasItemFlag(ItemFlag.HIDE_PLACED_ON)) ||
                                (!matrixMeta.hasItemFlag(ItemFlag.HIDE_PLACED_ON) && recipeMeta.hasItemFlag(ItemFlag.HIDE_PLACED_ON)) ||
                                (matrixMeta.hasItemFlag(ItemFlag.HIDE_POTION_EFFECTS) && !recipeMeta.hasItemFlag(ItemFlag.HIDE_POTION_EFFECTS)) ||
                                (!matrixMeta.hasItemFlag(ItemFlag.HIDE_POTION_EFFECTS) && recipeMeta.hasItemFlag(ItemFlag.HIDE_POTION_EFFECTS))) {
                            match = false; // mismatch item flags
                            break;
                        }
                        if ((matrixMeta.hasDisplayName() && !recipeMeta.hasDisplayName()) ||
                                (!matrixMeta.hasDisplayName() && recipeMeta.hasDisplayName()) ||
                                !matrixMeta.getDisplayName().equals(recipeMeta.getDisplayName())) {
                            match = false; // custom name mismatch
                            break;
                        }
                    }
                    continue;
                }
                match = false; // not same material and data
                break;
            }
            if (match) {
                return true; // this is a match
            }
        }
        return false; // no matching recipe found
    }
}
