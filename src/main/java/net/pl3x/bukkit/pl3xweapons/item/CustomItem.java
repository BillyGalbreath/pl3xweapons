package net.pl3x.bukkit.pl3xweapons.item;

import net.pl3x.bukkit.pl3xweapons.recipes.BaseRecipe;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public enum CustomItem {
    CAMPFIRE(Material.DIAMOND_HOE, 0, "Campfire", null), // Controlled by NewItems
    KEY(Material.DIAMOND_HOE, 1, "Key", null), // Controlled by MobDrops
    COIN(Material.DIAMOND_HOE, 2, "Coin", null), // Controlled by Pl3xMoney
    DOLLAR(Material.DIAMOND_HOE, 3, "Dollar", null), // Controlled by Pl3xMoney
    RUBY(Material.DIAMOND_HOE, 4, "Ruby", null);

    private static CustomItem[] BY_ID = new CustomItem[2];
    private static final Map<String, CustomItem> BY_NAME = new HashMap<>();

    static {
        for (CustomItem item : values()) {
            if (BY_ID.length > item.id) {
                BY_ID[item.id] = item;
            } else {
                BY_ID = Arrays.copyOfRange(BY_ID, 0, item.id + 2);
                BY_ID[item.id] = item;
            }
            BY_NAME.put(item.name(), item);
        }
    }

    public static CustomItem get(int id) {
        return BY_ID.length > id && id >= 0 ? BY_ID[id] : null;
    }

    public static CustomItem get(String name) {
        return BY_NAME.get(name);
    }

    public static CustomItem get(ItemStack item) {
        for (CustomItem value : values()) {
            if (value.isItem(item)) {
                return value;
            }
        }
        return null;
    }

    private ItemStack item;
    private int id;
    private String name;
    private BaseRecipe recipe;

    CustomItem(Material material, int id, String name, BaseRecipe recipe) {
        this.id = id;
        this.name = name;
        this.recipe = recipe;

        item = new ItemStack(material, 1, (short) (id + 101));

        ItemMeta meta = item.getItemMeta();
        meta.setUnbreakable(true);
        meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        meta.setDisplayName(ChatColor.WHITE + name);
        item.setItemMeta(meta);
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public BaseRecipe getRecipe() {
        return recipe;
    }

    public ItemStack getItem() {
        return item.clone();
    }

    public boolean isItem(ItemStack stack) {
        if (stack == null ||
                stack.getType() != item.getType() ||
                stack.getDurability() != 101 + id ||
                !stack.hasItemMeta()) {
            return false;
        }
        ItemMeta meta = stack.getItemMeta();
        return meta.isUnbreakable() &&
                meta.hasItemFlag(ItemFlag.HIDE_UNBREAKABLE);
    }
}
